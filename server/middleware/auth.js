const jwt = require('jsonwebtoken')

const checkToken = auth => (req, res, next) => {
  if (!auth) {
    next()
    return
  }

  const token = req.headers['authorization']
  let jwtPayload

  try {
    jwtPayload = jwt.verify(token, 'SECRET')
    res.locals.jwtPayload = jwtPayload
  } catch (err) {
    res.status(401).send(err)
    return
  }

  next()
}

const checkRole = roles => async (req, res, next) => {
  if (!roles) {
    next()
    return
  }

  const $id = res.locals.jwtPayload.roleId

  if (roles.indexOf($id) > -1) next()
  else res.status(401).send()
}

module.exports = {
  checkToken,
  checkRole,
}
