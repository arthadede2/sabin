const { createConnection } = require('typeorm')
const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const next = require('next')
const { checkToken, checkRole } = require('./middleware/auth')

const ApiRoutes = require('../app/routes')
const routes = require('../src/routes')
const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev, dir: './src' })
const handler = routes.getRequestHandler(app)

const prefixApi = '/api'

createConnection()
  .then(async connection => {
    app.prepare().then(() => {
      const server = express()
      server.use(morgan('dev'))
      server.use(bodyParser.json())
      server.use(bodyParser.urlencoded({ extended: true }))

      ApiRoutes.forEach(route => {
        server[route.method](
          prefixApi + route.path,
          [checkToken(route.auth), checkRole(route.role)],
          (request, response, next) => {
            route
              .action(request, response)
              .then(() => next)
              .catch(err => next(err))
          },
        )
      })

      server.use(handler)
      server.listen(port)
    })
  })
  .catch(err => console.log('TypeORM connection error: ', err))
