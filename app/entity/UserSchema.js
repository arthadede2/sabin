const { EntitySchema } = require('typeorm')
const User = require('../model/User')

module.exports = new EntitySchema({
  name: 'User',

  target: User,

  columns: {
    id: { primary: true, type: 'int', generated: true },
    name: { type: 'varchar' },
    email: { type: 'varchar' },
    password: { type: 'varchar' },
  },

  relations: {
    role: { target: 'Role', type: 'many-to-one' },
    models: { target: 'Model', type: 'one-to-many' },
  },
})
