const { EntitySchema } = require('typeorm')
const Label = require('../model/Label')

module.exports = new EntitySchema({
  name: 'Label',
  target: Label,
  columns: {
    id: {
      primary: true,
      type: 'int',
      generated: true,
    },
    name: {
      type: 'varchar',
      unique: true,
    },
  },
  relations: {
    models: {
      target: 'Model',
      type: 'many-to-many',
    },
  },
})
