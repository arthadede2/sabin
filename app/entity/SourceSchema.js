const { EntitySchema } = require('typeorm')
const Source = require('../model/Source')

module.exports = new EntitySchema({
  name: 'Source',
  target: Source,
  columns: {
    id: {
      primary: true,
      type: 'int',
      generated: true,
    },
    text: { type: 'text' },
  },
})
