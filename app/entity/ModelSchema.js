const { EntitySchema } = require('typeorm')
const Model = require('../model/Model')

module.exports = new EntitySchema({
  name: 'Model',
  target: Model,
  columns: {
    id: {
      primary: true,
      type: 'int',
      generated: true,
    },
    name: {
      type: 'varchar',
    },
    annotator: {
      type: 'enum',
      enum: ['classifier', 'extractor'],
    },
  },
  relations: {
    labels: {
      target: 'Label',
      type: 'many-to-many',
      nullable: true,
      joinTable: true,
      cascade: true,
    },
    module: {
      target: 'Module',
      type: 'many-to-one',
      nullable: true,
    },
    user: {
      target: 'User',
      type: 'many-to-one',
    },
  },
})
