const { EntitySchema } = require('typeorm')
const Module = require('../model/Module')

module.exports = new EntitySchema({
  name: 'Module',
  target: Module,
  columns: {
    id: {
      primary: true,
      type: 'int',
      generated: true,
    },
    name: { type: 'varchar' },
    desc: { type: 'varchar' },
    annotator: { type: 'enum', enum: ['classifer', 'extractor'] },
  },
  relations: {
    labels: {
      target: 'Label',
      type: 'many-to-many',
      joinTable: true,
    },
    models: {
      target: 'Model',
      type: 'one-to-many',
    },
  },
})
