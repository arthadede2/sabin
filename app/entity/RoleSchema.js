const { EntitySchema } = require('typeorm')
const Role = require('../model/Role')

module.exports = new EntitySchema({
  name: 'Role',
  target: Role,
  columns: {
    id: {
      primary: true,
      type: 'int',
      generated: true,
    },
    name: { type: 'varchar' },
  },
  relations: {
    users: {
      target: 'User',
      type: 'one-to-many',
    },
  },
})
