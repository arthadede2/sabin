const Query = require('../utils/Query')
const Module = require('../model/Module')
const Role = require('../model/Role')
const User = require('../model/User')

const modelFetch = async (req, res) => {
  try {
    const result = await Query.findAll('Model', {
      relations: ['labels', 'module', 'user'],
    })

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const modelUpdate = async (req, res) => {
  const $id = req.params.id
  const { name, annotator } = req.body
  const labelId = req.body.label
  const moduleId = req.body.module

  try {
    let model = await Query.findById('Model', $id)

    model.name = name
    model.annotator = annotator
    model.labels = labelId ? await Query.findByIds('Label', labelId) : undefined
    model.module = moduleId ? await Query.findById('Module', moduleId) : undefined

    const result = await Query.save('Model', model)

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const modelDestroy = async (req, res) => {
  const $id = req.params.id

  try {
    const model = await Query.findById('Model', $id)

    const result = Query.destroy('Model', model)

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const labelUpdate = async (req, res) => {
  const $id = req.params.id
  const { name } = req.body

  try {
    let label = await Query.findById('Label', $id)

    label.name = name

    const result = await Query.save('Label', label)
    return res.status(200).send(result)
  } catch (err) {
    return res.status(400).send(err)
  }
}

const labelDestroy = async (req, res) => {
  const $id = req.params.id

  try {
    const label = await Query.findById('Label', $id)

    const result = Query.destroy('Label', label)

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const moduleInsert = async (req, res) => {
  const { name, desc, annotator } = req.body
  const labelId = req.body.label

  try {
    let module = new Module()

    module.name = name
    module.desc = desc
    module.annotator = annotator
    module.labels = labelId ? await Query.findByIds('Label', labelId) : undefined

    const result = await Query.save('Module', module)

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const moduleUpdate = async (req, res) => {
  const $id = req.params.id
  const { name, desc, annotator } = req.body
  const labelId = req.body.label

  try {
    const module = await Query.findById('Module', $id)

    module.name = name
    module.desc = desc
    module.annotator = annotator
    module.labels = labelId ? await Query.findByIds('Label', labelId) : undefined

    const result = await Query.save('Module', module)

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const moduleDestroy = async (req, res) => {
  const $id = req.params.id

  try {
    const module = await Query.findById('Module', $id)

    const result = Query.destroy('Module', module)

    return res.status(200).send(result)
  } catch (err) {
    return res.status(400).send(err)
  }
}

const roleFetch = async (req, res) => {
  try {
    const result = await Query.findAll('Role')

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const roleInsert = async (req, res) => {
  const { name } = req.body
  try {
    let role = new Role()

    role.name = name

    const result = await Query.save('Role', role)

    return res.status(200).send(result)
  } catch (err) {
    return res.status(400).send(err)
  }
}

const roleUpdate = async (req, res) => {
  const $id = req.params.id
  const { name } = req.body

  try {
    let role = await Query.findById('Role', $id)

    role.name = name

    const result = await Query.save('Role', role)

    return res.status(200).send(result)
  } catch (err) {
    return res.status(400).send(err)
  }
}

const roleDestroy = async (req, res) => {
  const $id = req.params.id
  try {
    const role = await Query.findById('Role', $id)

    const result = Query.destroy('Role', role)

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const sourceDestroy = async (req, res) => {
  const $id = req.params.id

  try {
    const source = await Query.findById('Source', $id)

    const result = Query.destroy('Source', source)

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const userFetch = async (req, res) => {
  try {
    const result = await Query.findAll('User', {
      relations: ['role'],
    })

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const userInsert = async (req, res) => {
  const { name, email, password } = req.body
  const roleId = req.body.role

  try {
    let user = new User()
    user.name = name
    user.email = email
    user.password = password
    user.role = await Query.findById('Role', roleId)

    user.hashPassword()

    const result = await Query.save('User', user)

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const userUpdate = async (req, res) => {
  const $id = req.params.id
  const { name } = req.body
  const roleId = req.body.role

  try {
    let user = await Query.findById('User', $id)
    user.name = name
    user.role = roleId ? await Query.findById('Role', roleId) : undefined

    const result = await Query.save('User', user)

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const userDestroy = async (req, res) => {
  const $id = req.params.id

  try {
    const user = await Query.findById('User', $id)

    const result = Query.destroy('User', user)

    return res.status(200).send(result)
  } catch (err) {
    return res.status(400).send(err)
  }
}

module.exports = {
  modelFetch,
  modelUpdate,
  modelDestroy,
  labelUpdate,
  labelDestroy,
  moduleInsert,
  moduleUpdate,
  moduleDestroy,
  roleFetch,
  roleInsert,
  roleUpdate,
  roleDestroy,
  sourceDestroy,
  userFetch,
  userInsert,
  userUpdate,
  userDestroy,
}
