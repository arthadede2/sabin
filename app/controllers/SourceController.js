const Query = require('../utils/Query')
const Source = require('../model/Source')

const fetch = async (req, res) => {
  try {
    const result = await Query.findAll('Source')

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const insert = async (req, res) => {
  let { text } = req.body

  try {
    let source = new Source()

    source.text = text

    const result = await Query.save('Source', source)

    return res.status(200).send(result)
  } catch (err) {
    return res.status(400).send(err)
  }
}

module.exports = {
  fetch,
  insert,
}
