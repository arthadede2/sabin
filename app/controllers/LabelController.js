const Query = require('../utils/Query')
const Label = require('../model/Label')

const fetch = async (req, res) => {
  try {
    const result = await Query.findAll('Label')

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const insert = async (req, res) => {
  const { name } = req.body

  try {
    let label = new Label()

    label.name = name

    const result = await Query.save('Label', label)

    return res.status(200).send(result)
  } catch (err) {
    return res.status(400).send(err)
  }
}

module.exports = {
  fetch,
  insert,
}
