const Query = require('../utils/Query')
const Model = require('../model/Model')

const fetch = async (req, res) => {
  const userId = res.locals.jwtPayload.userId

  try {
    const result = await Query.findAll('Model', {
      relations: ['labels', 'module', 'user'],
      where: { user: { id: userId } },
    })

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const insert = async (req, res) => {
  const { name, desc, annotator } = req.body
  const labelId = req.body.label
  const moduleId = req.body.module
  const userId = res.locals.jwtPayload.userId

  try {
    let model = new Model()

    model.name = name
    model.desc = desc
    model.annotator = annotator
    model.user = await Query.findById('User', userId)
    model.labels = labelId ? await Query.findById('Label', labelId) : undefined
    model.module = moduleId
      ? await Query.findById('Module', moduleId)
      : undefined

    const result = await Query.save('Model', model)

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const update = async (req, res) => {
  const $id = req.params.id
  const { name, desc, annotator } = req.body
  const labelId = req.body.label
  const moduleId = req.body.module
  const userId = res.locals.jwtPayload.userId

  try {
    let model = await Query.findById('Model', $id, {
      where: { user: { id: userId } },
    })

    model.name = name
    model.desc = desc
    model.annotator = annotator
    model.labels = labelId ? await Query.findByIds('Label', labelId) : undefined
    model.module = moduleId
      ? await Query.findById('Module', moduleId)
      : undefined

    const result = await Query.save('Model', model)

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

const destroy = async (req, res) => {
  const $id = req.params.id
  const userId = res.locals.jwtPayload.userId

  try {
    const model = await Query.findById('Model', $id, {
      where: { user: { id: userId } },
    })

    const result = Query.destroy('Model', model)

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

module.exports = {
  fetch,
  insert,
  update,
  destroy,
}
