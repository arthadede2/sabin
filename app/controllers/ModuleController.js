const Query = require('../utils/Query')

const fetch = async (req, res) => {
  try {
    const result = await Query.findAll('Module', {
      relations: ['labels'],
    })

    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

module.exports = {
  fetch,
}
