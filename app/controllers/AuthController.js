const Query = require('../utils/Query')
const User = require('../model/User')
const jwt = require('jsonwebtoken')
const { query } = Query

const login = async (req, res) => {
  const { email, password } = req.body
  let auth = {}

  const user = await query('User').findOneOrFail({
    where: { email },
    relations: ['role'],
  })

  if (!user.checkPassword(password)) throw new Error('Password invalid')

  const token = jwt.sign(
    {
      userId: user.id,
      name: user.name,
      email: user.email,
      roleId: user.role.id,
      roleName: user.role.name,
    },
    'SECRET',
    { expiresIn: '1h' },
  )

  auth.token = token

  res.status(200).send(auth)
}

const register = async (req, res) => {
  const { name, email, password } = req.body
  try {
    let user = new User()

    user.name = name
    user.email = email
    user.password = password
    user.role = await Query.findById('Role', 2)
    user.hashPassword()

    const result = await Query.save('User', user)
    res.status(200).send(result)
  } catch (err) {
    res.status(400).send(err)
  }
}

module.exports = {
  login,
  register,
}
