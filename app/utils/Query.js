const { getManager } = require('typeorm')

class Query {
  static query(model) {
    return getManager().getRepository(model)
  }

  static findById(model, id, options) {
    return this.query(model).findOneOrFail(id, options)
  }

  static findByIds(model, ids, options) {
    return this.query(model).findByIds([...ids], options)
  }

  static findAll(model, options) {
    return this.query(model).find(options)
  }

  static save(model, body) {
    return this.query(model).save(body)
  }

  static destroy(model, data) {
    return this.query(model).delete(data.id)
  }
}

module.exports = Query
