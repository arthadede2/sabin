class Module {
  constructor(id, name, desc, annotator, labels, annotations) {
    this.id = id
    this.name = name
    this.desc = desc
    this.annotator = annotator
    this.labels = labels
    this.annotations = annotations
  }
}

module.exports = Module
