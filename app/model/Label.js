class Label {
  constructor(id, name, models) {
    this.id = id
    this.name = name
    this.models = models
  }
}

module.exports = Label
