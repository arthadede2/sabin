/* export */ class Model {
  constructor(id, name, annotator, module, labels, user) {
    this.id = id
    this.name = name
    this.annotator = annotator
    this.module = module
    this.labels = labels
    this.user = user
  }
}

module.exports = Model
