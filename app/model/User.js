const bcrypt = require('bcryptjs')
const salt = bcrypt.genSaltSync(8)

class User {
  constructor(id, name, email, password, role, models) {
    this.id = id
    this.name = name
    this.email = email
    this.password = password
    this.role = role
    this.models = models
  }

  hashPassword() {
    this.password = bcrypt.hashSync(this.password, salt)
  }

  checkPassword(password) {
    return bcrypt.compareSync(password, this.password)
  }
}

module.exports = User
