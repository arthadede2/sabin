const AuthController = require('./controllers/AuthController')
const LabelController = require('./controllers/LabelController')
const ModuleController = require('./controllers/ModuleController')
const ModelController = require('./controllers/ModelController')
const SourceController = require('./controllers/SourceController')
const AdminController = require('./controllers/AdminController')

const publicRoutes = [
  { path: '/login', method: 'post', action: AuthController.login },
  { path: '/register', method: 'post', action: AuthController.register },
]
const userRoutes = [
  { path: '/label', method: 'get', action: LabelController.fetch },
  { path: '/label', method: 'post', action: LabelController.insert },
  { path: '/module', method: 'get', action: ModuleController.fetch },
  { path: '/model', method: 'get', action: ModelController.fetch },
  { path: '/model', method: 'post', action: ModelController.insert },
  { path: '/model/:id', method: 'patch', action: ModelController.update },
  { path: '/model/:id', method: 'delete', action: ModelController.destroy },
  { path: '/source', method: 'get', action: SourceController.fetch },
  { path: '/source', method: 'post', action: SourceController.insert },
].map(route => ({ ...route, auth: true }))

const adminRoute = '/admin'
const adminRoutes = [
  { path: adminRoute + '/model', method: 'get', action: AdminController.modelFetch },
  { path: adminRoute + '/model', method: 'post', action: ModelController.insert },
  { path: adminRoute + '/model/:id', method: 'patch', action: AdminController.modelUpdate },
  { path: adminRoute + '/model/:id', method: 'delete', action: AdminController.modelDestroy },

  { path: adminRoute + '/label', method: 'get', action: LabelController.fetch },
  { path: adminRoute + '/label', method: 'post', action: LabelController.insert },
  { path: adminRoute + '/label/:id', method: 'patch', action: AdminController.labelUpdate },
  { path: adminRoute + '/label/:id', method: 'delete', action: AdminController.labelDestroy },

  { path: adminRoute + '/module', method: 'get', action: ModelController.fetch },
  { path: adminRoute + '/module', method: 'post', action: AdminController.moduleInsert },
  { path: adminRoute + '/module/:id', method: 'patch', action: AdminController.moduleUpdate },
  { path: adminRoute + '/module/:id', method: 'delete', action: AdminController.moduleDestroy },

  { path: adminRoute + '/role', method: 'get', action: AdminController.roleFetch },
  { path: adminRoute + '/role', method: 'post', action: AdminController.roleInsert },
  { path: adminRoute + '/role/:id', method: 'patch', action: AdminController.roleUpdate },
  { path: adminRoute + '/role/:id', method: 'delete', action: AdminController.roleDestroy },

  { path: adminRoute + '/source', method: 'get', action: SourceController.fetch },
  { path: adminRoute + '/source', method: 'post', action: SourceController.insert },
  { path: adminRoute + '/source/:id', method: 'delete', action: AdminController.sourceDestroy },

  { path: adminRoute + '/user', method: 'get', action: AdminController.userFetch },
  { path: adminRoute + '/user', method: 'post', action: AdminController.userInsert },
  { path: adminRoute + '/user/:id', method: 'patch', action: AdminController.userUpdate },
  { path: adminRoute + '/user/:id', method: 'delete', action: AdminController.userDestroy },
].map(route => ({ ...route, auth: true, role: [1] }))

module.exports = [...publicRoutes, ...userRoutes, ...adminRoutes]
