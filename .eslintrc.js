module.exports = {
    extends: ['airbnb-base', 'prettier', 'prettier/react'],
    plugins: ['prettier'],
    parser: ['babel-eslint'],
};
