import React from 'react'
import { Layout, Menu, Icon } from 'antd'

function Sider() {
  return (
    <Layout.Sider
      className="myd-sider"
      trigger={null}
      collapsible
      collapsed={false}
    >
      <Menu
        className="myd-sider-menu"
        mode="inline"
        defaultSelectedKeys={['1']}
      >
        <Menu.Item key="1">
          <Icon type="dashboard" />
          <span>Dashboard</span>
        </Menu.Item>
        <Menu.Item key="2">
          <Icon type="video-camera" />
          <span>Explore</span>
        </Menu.Item>
        <Menu.Item key="3">
          <Icon type="upload" />
          <span>Model</span>
        </Menu.Item>
        <Menu.Item key="4">
          <Icon type="upload" />
          <span>Source</span>
        </Menu.Item>
      </Menu>
    </Layout.Sider>
  )
}

export default Sider
