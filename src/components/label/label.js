import React, { useState, useEffect } from 'react'
import { Icon } from 'antd'

function Label(props) {
  const {
    children,
    color = '#747d8c',
    editable = false,
    onChange,
    onRemove,
    ...rest
  } = props
  const [state, setState] = useState(children)

  useEffect(() => {
    onChange(state)
  }, [state])

  if (editable) {
    return (
      <span
        style={{
          color,
          backgroundColor: color + '26',
          border: `solid 1px ${color + '66'}`,
          paddingRight: 30,
        }}
        className="anno-label"
      >
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <input
            type="text"
            className="anno-label-editable"
            value={state}
            onChange={e => setState(e.target.value)}
          />
          <Icon
            type="close"
            style={{
              color,
              right: 0,
              position: 'absolute',
              marginRight: 8,
              width: 30,
            }}
            onClick={() => onRemove()}
          />
        </div>
        <style jsx>{`
          .anno-label {
            display: inline-block;
            padding: 5px 9px;
            margin-left: 8px;
            border-radius: 3px;
          }

          .anno-label-editable {
            border: none;
            background: transparent;
            padding: 0;
            margin: 0;
          }
          .anno-label-editable:focus {
            outline: none;
          }
        `}</style>
      </span>
    )
  }

  return (
    <span
      className="anno-label"
      style={{
        color,
        backgroundColor: color + '26',
        border: `solid 1px ${color + '66'}`,
      }}
      {...rest}
    >
      {children}
      <style jsx>{`
        .anno-label {
          display: inline-block;
          padding: 5px 9px;
          line-height: 1;
          margin-left: 8px;
          border-radius: 3px;
        }
      `}</style>
    </span>
  )
}

export default Label
