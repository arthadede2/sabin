import React from 'react'

function Radio(props) {
  const { children, value } = props
  return (
    <div className="anno-radio-wrapper">
      <span className="anno-radio">
        <input
          type="radio"
          className="anno-radio-input"
          value={value}
        />
      </span>
      {children}
      <style jsx>{`
        .anno-radio {
        }
      `}</style>
    </div>
  )
}

export default Radio
