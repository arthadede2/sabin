import React, { Component } from 'react'

class RadioGroup extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { children } = this.props
    return (
      <div className="anno-radio-group">
        {children}
        <style>{`
        .anno-radio-group {
          display: block;
        }
      `}</style>
      </div>
    )
  }
}

export default RadioGroup
