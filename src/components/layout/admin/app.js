import React from 'react'
import { Layout } from 'antd'
import Header from './header'
import Sider from './sider'
import Content from './content'

function App(props) {
  return (
    <Layout className="myd-layout">
      <Header />
      <Layout className="myd-layout-wrapper">
        <Sider />
        <Content>{props.children}</Content>
      </Layout>
    </Layout>
  )
}

export default App
