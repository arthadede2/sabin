import React from 'react'
import { Layout, Button, Icon } from 'antd'

function Header() {
  return (
    <Layout.Header className="myd-header">
      <div className="logo" />
      <div>
        <Button shape="circle">
          <Icon
            className="trigger"
            type={true ? 'menu-unfold' : 'menu-fold'}
            onClick={() => console.log('modified')}
          />
        </Button>
      </div>
    </Layout.Header>
  )
}

export default Header
