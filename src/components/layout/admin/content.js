import React from 'react'
import { Breadcrumb } from 'antd'

function Content(props) {
  return (
    <div className="myd-content">
      <div className="myd-content-header">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="">Application Center</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="">Application List</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>An Application</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="myd-content-wrapper">{props.children}</div>
    </div>
  )
}

export default Content
