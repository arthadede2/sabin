import React from 'react'

function Button(props) {
  const {
    variant = 'button',
    type = 'solid',
    children,
    ...rest
  } = props
  const Component = variant
  return (
    <Component className={`anno-btn ${'anno-btn-' + type}`} {...rest}>
      {children}
      <style jsx>{`
        .anno-btn {
          background: #f26c4f;
          font-size: 16px;
          line-height: 1;
          padding: 10px 30px;
          text-decoration: none;
          outline: none;
          cursor: pointer;
        }

        .anno-btn-solid {
          border: none;
          color: #fff;
        }

        .anno-btn-outlined {
          border: solid 0.3px #f26c4f;
          background: none;
          color: #f26c4f;
          transition: all 0.4s ease;
        }

        .anno-btn-outlined:hover {
          background: #f26c4f;
          color: #fff;
        }
      `}</style>
    </Component>
  )
}

export default Button
