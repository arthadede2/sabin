import React from 'react'

function Content(props) {
  return (
    <div className="myd-content">
      {props.header && <div className="myd-content-header">{props.header}</div>}
      <div className="myd-content-wrapper">{props.children}</div>
    </div>
  )
}

export default Content
