import React from 'react'
import { Layout } from 'antd'

function Header(props) {
  return (
    <Layout.Header className="myd-header">
      <div className="logo" />
      <div className="myd-header-items">{props.items}</div>
    </Layout.Header>
  )
}

export default Header
