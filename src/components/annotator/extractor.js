import React, { useState, useEffect } from 'react'
import _ from 'lodash'

function Extractor(props) {
  const { source: state, onLabel, onChange: setState } = props

  // Handle onClick for node have a label
  const handleClick = props => {
    const { index, label, text: items } = props

    let tempState = state // make var temporary for state

    if (label !== onLabel) {
      tempState[index].label = onLabel // change value of label if had same label

      let beforeNode = tempState[index - 1] || {} // make temporary from node before
      let afterNode = tempState[index + 1] || {} // make temporary from node after

      if (
        beforeNode.hasOwnProperty('label') &&
        beforeNode.label === onLabel &&
        afterNode.hasOwnProperty('label') &&
        afterNode.label === onLabel
      ) {
        beforeNode.text = [...beforeNode.text, ...items, ...afterNode.text] // beforeNode has marge with that node text and afterNode text

        tempState.splice(index, 2) // slice 2 items start from index

        return setState([...tempState])
      }

      if (beforeNode.hasOwnProperty('label') && beforeNode.label === onLabel) {
        beforeNode.text = [...beforeNode.text, ...items] // beforeNode has marge with that node text

        tempState.splice(index, 1)

        return setState([...tempState])
      }

      if (afterNode.hasOwnProperty('label') && afterNode.label === onLabel) {
        afterNode.text = [...items, ...afterNode.text] // afterNode has marge with that node text

        tempState.splice(index, 1)

        return setState([...tempState])
      }
    }

    if (label === onLabel) {
      const nodeText = items.map(item => ({ text: [item] }))
      tempState.splice(index, 1, ...nodeText)
    }

    return setState([...tempState])
  }

  // Handle onClick for node don't have label
  const handleMarge = props => {
    const { index, text: items } = props

    // Make temporary for state
    let tempState = state

    // Make BeforeNode AfterNode Index
    let beforeNode = tempState[index - 1] || {}
    let afterNode = tempState[index + 1] || {}

    //  BeforeNode AfterNode join
    if (
      beforeNode.hasOwnProperty('label') &&
      beforeNode.label === onLabel &&
      afterNode.hasOwnProperty('label') &&
      afterNode.label === onLabel
    ) {
      beforeNode.text = [...beforeNode.text, ...items, ...afterNode.text]

      tempState.splice(index, 2)

      return setState([...tempState])
    }

    //  BeforeNode join
    if (beforeNode.hasOwnProperty('label') && beforeNode.label === onLabel) {
      beforeNode.text = [...beforeNode.text, ...items]

      tempState.splice(index, 1)

      return setState([...tempState])
    }

    //  AfterNode join
    if (afterNode.hasOwnProperty('label') && afterNode.label === onLabel) {
      afterNode.text = [...items, ...afterNode.text]

      tempState.splice(index, 1)

      return setState([...tempState])
    }

    // Create new NodeLabel
    tempState[index].label = onLabel

    return setState([...tempState])
  }

  return (
    <div
      style={{
        display: 'flex',
        flexWrap: 'wrap',
        maxHeight: 340,
        overflow: 'auto',
      }}
    >
      {state.map((node, index, arr) => {
        if (node.label) {
          const labelText = node.label
          return (
            <div
              key={index}
              className="anno-select-span anno-select-wrapper"
              onClick={() => handleClick({ ...node, index })}
            >
              <div className="anno-select">
                <span style={{ margin: '4px' }}>{node.text.join(' ')}</span>
                <span className="anno-select-label">{labelText}</span>
              </div>
            </div>
          )
        }
        return (
          <span
            key={index}
            style={{ margin: 4 }}
            className="anno-select-span"
            onClick={() => handleMarge({ ...node, index })}
          >
            {node.text}
          </span>
        )
      })}
      <style jsx>{`
        .anno-select-wrapper {
          margin: 0px 8px;
          padding: 2px 8px;
        }
        .anno-select-wrapper,
        .anno-select-span {
          cursor: pointer;
        }
        .anno-select {
          height: 25px;
          display: flex;
          color: rgba(9, 132, 227, 1);
          align-items: center;
          background: rgba(9, 132, 227, 0.1);
          border: solid 1px rgba(9, 132, 227, 0.4);
          border-radius: 3px;
          padding: 0px 8px;
        }
        .anno-select-label {
          background: rgb(9, 132, 227);
          color: #fff;
          font-size: 10px;
          font-weight: 600;
          border-radius: 3px;
          padding: 0px 8px;
          margin-left: 8px;
        }
      `}</style>
    </div>
  )
}

export default Extractor
