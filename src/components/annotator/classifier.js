import React, { useEffect } from 'react'

function classifier(props) {
  const { value } = props

  return (
    <div
      style={{
        display: 'flex',
        flexWrap: 'wrap',
        lineHeight: 2,
        fontSize: 16,
        maxHeight: 340,
        overflow: 'auto',
        margin: 4,
        cursor: 'default',
      }}
    >
      <span>{value}</span>
    </div>
  )
}

export default classifier
