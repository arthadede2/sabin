import Card from './card'
import CardContainer from './card-container'
import CardFooter from './card-footer'
import CardTitle from './card-title'
import CardIcon from './card-icon'
import CardDesc from './card-desc'

Card.Container = CardContainer
Card.Footer = CardFooter
Card.Icon = CardIcon
Card.Title = CardTitle
Card.Title = CardTitle
Card.Desc = CardDesc

export default Card
