import { Icon } from 'antd'

function CardIcon(props) {
  const { children, src } = props
  return (
    <div className="myd-card-icon">
      <img src={src} />
    </div>
  )
}

export default CardIcon
