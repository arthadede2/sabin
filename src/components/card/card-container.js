import React from 'react'

function CardContainer({
  align = 'flex-start',
  justify = 'flex-start',
  children,
}) {
  return (
    <div
      className="myd-card-container"
      style={{ alignItems: align, justifyContent: justify }}
    >
      {children}
    </div>
  )
}

export default CardContainer
