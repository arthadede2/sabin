import React from 'react'
function CardModel(props) {
  const { children, ...rest } = props

  return (
    <div className="myd-card" {...rest}>
      {children}
    </div>
  )
}

export default CardModel
