import React from 'react'

function CardDesc(props) {
  const { align = 'initial', children, ...rest } = props
  return (
    <p className="anno-card-desc" {...rest}>
      {children}
      <style jsx>{`
        .anno-card-desc {
          display: -webkit-box;
          white-space: normal;
          overflow: hidden;
          text-overflow: ellipsis;
          -webkit-line-clamp: 3;
          -webkit-box-orient: vertical;
          color: #666666;
          font-size: 14px;
          font-weight: 400;
          text-align: ${align};
        }
      `}</style>
    </p>
  )
}

export default CardDesc
