import React from 'react'

function CardFooter({ children, ...rest }) {
  return (
    <div className="myd-card-footer" {...rest}>
      {children}
    </div>
  )
}

export default CardFooter
