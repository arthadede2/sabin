import React from 'react'

function CardTitle({ children }) {
  return <h5 className="myd-card-title">{children}</h5>
}

export default CardTitle
