import React, { useState } from 'react'
import Axios from 'axios'
import Card from '../components/card'
import CenterLayout from '../containers/CenterLayout'
import { Router } from '../routes'
import { Typography, Form, Icon, Input, Button, Row, Col } from 'antd'

function Register(props) {
  const url = props.apiUrl
  const [error, setError] = useState(null)
  const { getFieldDecorator, validateFields } = props.form

  const handleSubmit = e => {
    e.preventDefault()
    validateFields(async (err, values) => {
      if (!err) {
        let data = {}

        data.email = values.email
        data.password = values.password
        data.name = `${values.firstname} ${values.lastname}`

        try {
          const response = await Axios({
            method: 'POST',
            url,
            data,
          })
          Router.pushRoute('login')
        } catch (err) {
          setError(err)
        }
      }
    })
  }

  const handleLogin = e => {
    e.preventDefault()
    Router.pushRoute('login')
  }

  return (
    <div className="myd-content-center">
      <Card style={{ padding: 60, marginBottom: 8 }}>
        <Typography.Title level={4} style={{ marginBottom: 30 }}>
          Register
        </Typography.Title>
        <Form
          onSubmit={handleSubmit}
          layout="vertical"
          className="login-form-wrapper"
        >
          <div className="login-form-item">
            {getFieldDecorator('email', {
              rules: [
                { required: true, message: 'Please input your username!' },
              ],
            })(<Input placeholder="Email address" />)}
          </div>
          <div className="login-form-item">
            <Row gutter={16}>
              <Col xs={12}>
                {getFieldDecorator('firstname', {
                  rules: [
                    { required: true, message: 'Please input your username!' },
                  ],
                })(<Input placeholder="First Name" />)}
              </Col>
              <Col xs={12}>
                {getFieldDecorator('lastname', {
                  rules: [
                    { required: true, message: 'Please input your username!' },
                  ],
                })(<Input placeholder="Last Name" />)}
              </Col>
            </Row>
          </div>
          <div className="login-form-item">
            {getFieldDecorator('password', {
              rules: [
                { required: true, message: 'Please input your Password!' },
              ],
            })(<Input type="password" placeholder="Password" />)}
          </div>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
            block
          >
            Register
          </Button>
        </Form>
      </Card>
      <Typography.Text>
        Already a member?{'  '}
        <a onClick={handleLogin}>Login</a>
      </Typography.Text>
    </div>
  )
}

Register.Layout = CenterLayout
Register.getInitialProps = async ({ req, res, isAuth }) => {
  if (isAuth) {
    if (req) {
      res.writeHead(302, { Location: '/' })
      res.end()
      return
    }

    Router.pushRoute('/')
  }

  const protocol = process.env.NODE_ENV === 'production' ? 'https' : 'http'

  const apiUrl = process.browser
    ? `${protocol}://${window.location.host}/api/register`
    : `${protocol}://${req.headers.host}/api/register`

  return { apiUrl }
}

const RegisterForm = Form.create({ name: 'register_form' })(Register)

export default RegisterForm
