import { useEffect } from 'react'
import Axios from 'axios'
import nextCookie from 'next-cookies'
import { Router } from '../routes'
import { connect } from 'react-redux'
import { setModel } from '../actions'
import { Typography, Button } from 'antd'
import UserLayout from '../containers/UserLayout'
import ModelUser from '../containers/ModelUser'

function Dashboard(props) {
  const modelArr = props.model.items

  const handleCreateModel = () => {
    Router.pushRoute('/create-model')
  }

  useEffect(() => {
    console.log(props)
  }, [])
  return (
    <div className="myd-container">
      <div className="myd-container-item">
        <div className="myd-container-item-header">
          <Typography.Title level={4}>Your Models</Typography.Title>
          <div className="myd-container-item-header-action">
            <Button type="primary" onClick={handleCreateModel}>
              Create Model
            </Button>
          </div>
        </div>
        <ModelUser data={modelArr} />
      </div>
    </div>
  )
}

Dashboard.getInitialProps = async ctx => {
  const { token } = nextCookie(ctx)

  const protocol = process.env.NODE_ENV === 'production' ? 'https' : 'http'

  const apiUrl = process.browser
    ? `${protocol}://${window.location.host}/api`
    : `${protocol}://${ctx.req.headers.host}/api`

  const redirectOnError = () =>
    process.browser
      ? Router.push('/login')
      : ctx.res.writeHead(302, { Location: '/login' }).end()

  try {
    const response = await Axios({
      method: 'GET',
      url: `${apiUrl}/model`,
      headers: {
        authorization: token,
      },
    })

    ctx.store.dispatch(setModel(response.data))
  } catch (err) {
    return redirectOnError()
  }
  return {}
}

Dashboard.Layout = UserLayout

export default connect(state => state)(Dashboard)
