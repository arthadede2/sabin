import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button, Divider } from 'antd'
import CustomButton from '../components/button'
import Card from '../components/card'
import { Router } from '../routes'

const editByIndex = (items, value, index) => {
  return items.map((item, i) => {
    if (index === i) return value
    return item
  })
}

function ManualSource(props) {
  const { query } = props
  const [editControl, setEditControl] = useState(null)
  const [state, setState] = useState('')
  const [stateArr, setStateArr] = useState([])
  let cardField = React.createRef()

  const handleFocus = () => {
    cardField.current.focus()
  }

  const handleNext = () => {
    Router.pushRoute('annotation', query)
  }

  const handleAdd = () => {
    setStateArr(stateArr => [...stateArr, state])
    setState('')
    handleFocus()
  }

  const handleEdit = i => {
    setEditControl(i)
    setState(stateArr[i])
  }

  const handleSaveEdit = () => {
    setStateArr(stateArr => editByIndex(stateArr, state, editControl))
    setEditControl(null)
    setState('')
    handleFocus()
  }

  const handleCancelEdit = () => {
    setEditControl(null)
    setState('')
    handleFocus()
  }

  const handleRemove = i => {
    setStateArr(state => state.filter((item, index) => index !== i))
    setState('')
    handleFocus()
  }

  useEffect(() => {
    handleFocus()
  }, [])

  return (
    <div
      style={{
        width: 900,
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <div style={{ background: '#fafaff' }}>
        <Row gutter={16}>
          <Col xs={16} style={{ height: 460 }}>
            <div
              style={{
                background: '#fff',
                boxShadow: '10px 0px 10px 1px hsla(0, 0%, 0%, 0.03)',
                height: 460,
              }}
            >
              <textarea
                ref={cardField}
                name=""
                id=""
                value={state}
                onChange={e => setState(e.target.value)}
                spellCheck="false"
                style={{
                  width: '100%',
                  height: 'inherit',
                  resize: 'none',
                  overflowY: 'auto',
                  border: 'none',
                  outline: 'none',
                  fontSize: 16,
                  lineHeight: 2,
                  padding: 40,
                }}
              />
              <div
                style={{
                  position: 'absolute',
                  bottom: 0,
                  height: 80,
                  paddingLeft: 40,
                  paddingRight: 56,
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                  width: '100%',
                }}
              >
                {editControl !== null && (
                  <Button
                    type="danger"
                    shape="circle"
                    icon="close"
                    size="large"
                    onClick={handleCancelEdit}
                    style={{ marginLeft: 16 }}
                  />
                )}
                {editControl !== null && (
                  <Button
                    type="primary"
                    shape="circle"
                    icon="save"
                    size="large"
                    onClick={handleSaveEdit}
                    style={{ marginLeft: 16 }}
                  />
                )}
                {editControl === null && (
                  <Button
                    type="primary"
                    shape="circle"
                    icon="plus"
                    size="large"
                    onClick={handleAdd}
                    style={{ marginLeft: 16 }}
                  />
                )}
              </div>
            </div>
          </Col>
          <Col xs={8} style={{ height: 460, overflow: 'auto' }}>
            <div
              style={{
                padding: '40px 30px',
                display: 'flex',
                height: 'auto',
                flexDirection: 'column',
              }}
            >
              {stateArr.map((item, index) => {
                if (index !== editControl) {
                  return (
                    <div key={index} style={{ position: 'relative' }}>
                      <div
                        className="anno-list"
                        style={{
                          height: 40,
                          overflow: 'hidden',
                          paddingRight: 24 + 16,
                        }}
                      >
                        <span
                          style={{
                            textOverflow: 'ellipsis',
                          }}
                        >
                          {item}
                        </span>
                        <div
                          className="anno-list-actions"
                          style={{
                            top: 0,
                            right: 0,
                            position: 'absolute',
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'flex-end',
                            justifyContent: 'center',
                          }}
                        >
                          <Button
                            type="primary"
                            icon="edit"
                            shape="circle"
                            size="small"
                            style={{
                              marginBottom: 2,
                            }}
                            onClick={() => handleEdit(index)}
                          />
                          <Button
                            type="danger"
                            icon="delete"
                            shape="circle"
                            size="small"
                            style={{ marginTop: 2 }}
                            onClick={() => handleRemove(index)}
                          />
                        </div>
                      </div>

                      <Divider />
                    </div>
                  )
                }
              })}
            </div>
          </Col>
        </Row>
      </div>
      <div>
        <CustomButton onClick={handleNext}>Lanjutkan</CustomButton>
      </div>
      <style jsx>{`
        .anno-list .anno-list-actions {
          opacity: 0;
          transition: all 0.3s ease;
        }
        .anno-list:hover .anno-list-actions {
          opacity: 1;
        }
      `}</style>
    </div>
  )
}
ManualSource.getInitialProps = ({ query }) => {
  return {
    query,
  }
}

export default ManualSource
