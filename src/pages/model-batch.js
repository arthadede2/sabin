import { Typography, Row, Col, List, Tag, Input, Table } from 'antd'
import Card from '../components/card'
import UserLayout from '../containers/UserLayout'
import SiderModel from '../containers/SiderModel'

const routeBuild = ['Training', 'Source', 'Stats', 'Report']
const routeRun = ['Demo', 'Batch', 'Stats']

function ModelBatch(props) {
  return (
    <div className="myd-container">
      <div className="myd-container-item">
        <Row gutter={32}>
          <Col md={3}>
            <SiderModel />
          </Col>
          <Col md={21}>
            <Typography.Title level={4}>Batch Processing</Typography.Title>
            <Card style={{ marginBottom: 16 }}>
              <Card.Container align="center" justify="center">
                <Typography.Text>Choose File..</Typography.Text>
              </Card.Container>
            </Card>
          </Col>
        </Row>
      </div>
    </div>
  )
}

ModelBatch.Layout = UserLayout

export default ModelBatch
