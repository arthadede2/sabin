import React from 'react'
import { Provider } from 'react-redux'
import App, { Container } from 'next/app'
import withRedux from 'next-redux-wrapper'
import initStore from '../store'

import '../assets/less/index.less'

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    const pageProps = Component.getInitialProps
      ? await Component.getInitialProps(ctx)
      : {}

    return { ...pageProps }
  }

  render() {
    const { Component, store, ...rest } = this.props
    return (
      <Container>
        <Provider store={store}>
          <Component.Layout>
            <div className="anno-content">
              <Component {...rest} />
            </div>
          </Component.Layout>
          <style global jsx>{`
            .anno-content {
              display: flex;
              justify-content: center;
              height: 100%;
              width: 100%;
            }
          `}</style>
        </Provider>
      </Container>
    )
  }
}

export default withRedux(initStore)(MyApp)
