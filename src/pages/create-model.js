import {
  Typography,
  Button,
  Form,
  Input,
  Radio,
  Icon,
  Row,
  Col,
  List,
  Upload,
} from 'antd'
import UserLayout from '../containers/UserLayout'
import Card from '../components/card'
const RadioButton = Radio.Button
let id = 0

function CreateModel(props) {
  const {
    getFieldDecorator,
    validateFields,
    getFieldValue,
    setFieldsValue,
  } = props.form

  const add = () => {
    const labels = getFieldValue('labels')
    const nextLabels = labels.concat(id++)
    setFieldsValue({
      labels: nextLabels,
    })
  }

  const remove = k => {
    const labels = getFieldValue('labels')
    if (labels.length === 1) {
      return
    }

    // can use data-binding to set
    setFieldsValue({
      labels: labels.filter(key => key !== k),
    })
  }

  getFieldDecorator('labels', { initialValue: [] })

  const labels = getFieldValue('labels')
  const labelItems = labels.map((k, index) => (
    <Form.Item required={false} key={k}>
      {getFieldDecorator(`names[${k}]`, {
        validateTrigger: ['onChange', 'onBlur'],
        rules: [
          {
            required: true,
            whitespace: true,
            message: 'Please input label or delete this field.',
          },
        ],
      })(
        <Input
          placeholder="Label name"
          style={{ width: '60%', marginRight: 8 }}
        />,
      )}
      {labels.length > 1 ? (
        <Icon
          className="dynamic-delete-button"
          type="minus-circle-o"
          onClick={() => remove(k)}
        />
      ) : null}
    </Form.Item>
  ))

  const handleSubmit = e => {
    e.preventDefault()
    validateFields(async (err, values) => {
      if (!err) {
        console.log(values)
      }
    })
  }

  return (
    <div className="myd-container">
      <div className="myd-container-item">
        <div className="myd-container-item-header">
          <Typography.Title level={4}>Create Model</Typography.Title>
        </div>
        <Form>
          <Row gutter={16}>
            <Col md={18}>
              <Card>
                <Card.Title>Model Information</Card.Title>
                <Form.Item label="Modal name">
                  {getFieldDecorator('name', {
                    rules: [
                      {
                        required: true,
                        message: 'Please input your model name!',
                      },
                    ],
                  })(<Input />)}
                </Form.Item>
                <Form.Item label="Modal description">
                  {getFieldDecorator('desc', {
                    rules: [
                      {
                        required: true,
                        message: 'Please input your model desc!',
                      },
                    ],
                  })(<Input.TextArea rows={4} />)}
                </Form.Item>
                <Form.Item label="Annotator">
                  {getFieldDecorator('annotator', {
                    rules: [
                      { required: true, message: 'Please choose annotator!' },
                    ],
                  })(
                    <Radio.Group>
                      <RadioButton value="classifier">Classifier</RadioButton>
                      <RadioButton value="extractor">Extractor</RadioButton>
                    </Radio.Group>,
                  )}
                </Form.Item>
                <Form.Item label="Mode">
                  {getFieldDecorator('modifier', {
                    initialValue: 'public',
                  })(
                    <Radio.Group>
                      <Radio value="public">Public</Radio>
                      <Radio value="private">Private</Radio>
                    </Radio.Group>,
                  )}
                </Form.Item>
                <Card.Footer
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}
                >
                  <Button type="primary" htmlType="submit">
                    Create
                  </Button>
                  <Button>Cancel</Button>
                </Card.Footer>
              </Card>
            </Col>
            <Col md={6}>
              <Card style={{ marginBottom: 16 }}>
                <Card.Title>Model Avatar</Card.Title>
                <Card.Container align="center">
                  <img
                    src="/static/icon/add.png"
                    alt="avatar"
                    className="myd-model-avatar"
                  />
                </Card.Container>
                <Form.Item label="Uploud avatar">
                  {getFieldDecorator('upload', {
                    valuePropName: 'fileList',
                  })(
                    <Upload name="logo" action="/upload.do" listType="picture">
                      <Button> Choose file..</Button>
                    </Upload>,
                  )}
                </Form.Item>
              </Card>
              <Card style={{ marginBottom: 16 }}>
                <Card.Title>Model Labels</Card.Title>
                {labelItems}
                <Form.Item>
                  <Button type="dashed" onClick={add}>
                    <Icon type="plus" /> Add field
                  </Button>
                </Form.Item>
              </Card>
            </Col>
          </Row>
        </Form>
      </div>
    </div>
  )
}

CreateModel.Layout = UserLayout
CreateModel.getInitialProps = async ctx => {
  return {}
}

const WrappedCreateModel = Form.create({ name: 'create-model' })(CreateModel)

export default WrappedCreateModel
