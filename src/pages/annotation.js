import { kebabCase } from 'lodash'
import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import Classifier from '../containers/Classifier'
import Extractor from '../containers/Extractor'

function Annotation(props) {
  const { model } = props
  useEffect(() => {
    console.log(props)
  }, [])

  if (model.annotator === 'classifier') {
    return <Classifier {...props} />
  }

  if (model.annotator === 'extractor') {
    return <Extractor {...props} />
  }

  return null
}
Annotation.getInitialProps = ({ query, store }) => {
  return {
    query,
  }
}

const mapStateToProps = (state, ownProps) => {
  const model = state.model.items.find(
    item => kebabCase(item.name) === ownProps.query.model
  )

  return {
    model,
  }
}

export default connect(
  mapStateToProps,
  null
)(Annotation)
