import {
  Typography,
  Row,
  Col,
  List,
  Tag,
  Input,
  Table,
  Button,
  Divider,
} from 'antd'
import Card from '../components/card'
import UserLayout from '../containers/UserLayout'
import SiderModel from '../containers/SiderModel'

const routeBuild = ['Training', 'Source', 'Stats', 'Report']
const routeRun = ['Demo', 'Batch', 'Stats']

function ModelData(props) {
  return (
    <div className="myd-container">
      <div className="myd-container-item">
        <Row gutter={32}>
          <Col md={3}>
            <SiderModel />
          </Col>
          <Col md={21}>
            <div
              style={{
                display: 'flex',
                flexDirection: 'row',
                marginBottom: 16,
              }}
            >
              <Input.Search
                placeholder="Input search text"
                onSearch={value => console.log(value)}
                style={{ width: 200, marginRight: 'auto' }}
              />
              <Button type="primary" style={{ marginLeft: 30 }}>
                Uploud Data
              </Button>
              <Button type="primary" style={{ marginLeft: 30 }}>
                Actions
              </Button>
            </div>
            <Divider />
            <Card style={{ marginBottom: 16 }}>
              <Card.Container align="center" justify="center">
                <Typography.Text>Table</Typography.Text>
              </Card.Container>
            </Card>
          </Col>
        </Row>
      </div>
    </div>
  )
}

ModelData.Layout = UserLayout

export default ModelData
