import React, { useState, useEffect } from 'react'
import SourceUploud from '../containers/SourceUploud'
import SourceSelect from '../containers/SourceSelect'
import Button from '../components/button'
import { Router } from '../routes'

const steps = [
  {
    title: 'Source Uploud',
    content: SourceUploud,
  },
  {
    title: 'Source Select',
    content: SourceSelect,
  },
]

function SourceFile(props) {
  const { query } = props
  const [pos, setPos] = useState(0)
  const [source, setSource] = useState({
    fileList: [],
    selected: [],
  })

  const Component = steps[pos].content

  const handlePosition = pos => {
    setPos(pos)
  }

  const handleNext = () => {
    Router.pushRoute('annotation', query)
  }

  return (
    <div style={{ width: 900, display: 'flex', flexDirection: 'column' }}>
      <div>
        <Component value={source} onChange={setSource} />
      </div>
      <div>
        {pos > 0 && (
          <Button onClick={() => handlePosition(pos - 1)}>Kembali</Button>
        )}
        {pos < steps.length - 1 && (
          <Button onClick={() => handlePosition(pos + 1)}>Lanjutkan</Button>
        )}
        {pos === steps.length - 1 && <Button onClick={handleNext}>Done</Button>}
      </div>
    </div>
  )
}

SourceFile.getInitialProps = ({ query }) => {
  return {
    query,
  }
}

export default SourceFile
