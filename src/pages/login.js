import React, { useState } from 'react'
import Axios from 'axios'
import { Router } from '../routes'
import { setToken } from '../utils/auth'
import { Typography, Form, Icon, Input, Button, Checkbox } from 'antd'
import CenterLayout from '../containers/CenterLayout'
import Card from '../components/card'

function Login(props) {
  const url = props.apiUrl
  const [error, setError] = useState(null)
  const { getFieldDecorator, validateFields } = props.form

  const handleSubmit = e => {
    e.preventDefault()
    validateFields(async (err, values) => {
      if (!err) {
        try {
          const response = await Axios({
            method: 'POST',
            url,
            data: values,
          })
          const { token } = response.data
          setToken(token)
          Router.pushRoute('dashboard')
        } catch (err) {
          setError(err)
        }
      }
    })
  }

  const handleRegister = e => {
    e.preventDefault()
    Router.pushRoute('register')
  }

  return (
    <div className="myd-content-center">
      <Card style={{ padding: 60, marginBottom: 8 }}>
        <Typography.Title level={4} style={{ marginBottom: 30 }}>
          Login
        </Typography.Title>
        <Form
          onSubmit={handleSubmit}
          layout="vertical"
          className="login-form-wrapper"
        >
          <div className="login-form-item">
            {getFieldDecorator('email', {
              rules: [{ required: true, message: 'Please input your Email!' }],
            })(
              <Input
                prefix={
                  <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />
                }
                placeholder="Email"
              />,
            )}
          </div>
          <div className="login-form-item">
            {getFieldDecorator('password', {
              rules: [
                { required: true, message: 'Please input your Password!' },
              ],
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />
                }
                type="password"
                placeholder="Password"
                style={{ marginBottom: 16 }}
              />,
            )}
            <a className="login-form-forgot" href="">
              Forgot password
            </a>
          </div>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
            block
          >
            Login
          </Button>
        </Form>
      </Card>
      <Typography.Text>
        Don't have an account?{'  '}
        <a onClick={handleRegister}>Sign Up</a>
      </Typography.Text>
    </div>
  )
}

Login.Layout = CenterLayout

Login.getInitialProps = async ({ req, res, isAuth }) => {
  if (isAuth) {
    if (req) {
      res.writeHead(302, { Location: '/' })
      res.end()
      return
    }

    Router.pushRoute('/')
  }

  const protocol = process.env.NODE_ENV === 'production' ? 'https' : 'http'

  const apiUrl = process.browser
    ? `${protocol}://${window.location.host}/api/login`
    : `${protocol}://${req.headers.host}/api/login`

  return { apiUrl }
}

const LoginForm = Form.create({ name: 'login_form' })(Login)

export default LoginForm
