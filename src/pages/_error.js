import BlankLayout from '../containers/BlankLayout'
function PageNotFound(props) {
  const { router } = props

  return (
    <div>
      <h2>NOT FOUND</h2>
    </div>
  )
}
PageNotFound.Layout = BlankLayout

export default PageNotFound
