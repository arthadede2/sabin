import { Typography, Row, Col, List, Tag, Input, Table } from 'antd'
import Card from '../components/card'
import UserLayout from '../containers/UserLayout'
import SiderModel from '../containers/SiderModel'

const routeBuild = ['Training', 'Source', 'Stats', 'Report']
const routeRun = ['Demo', 'Batch', 'Stats']

function ModelStats(props) {
  return (
    <div className="myd-container">
      <div className="myd-container-item">
        <Row gutter={32}>
          <Col md={3}>
            <SiderModel />
          </Col>
          <Col md={21}>
            <Row gutter={16}>
              <Col md={6}>
                <Card>
                  <Typography.Text>LABEL</Typography.Text>
                </Card>
              </Col>
              <Col md={18}>
                <Card>
                  <Card.Container align="center" justify="center">
                    <Typography.Text>ANNOTATION</Typography.Text>
                  </Card.Container>
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  )
}

ModelStats.Layout = UserLayout

export default ModelStats
