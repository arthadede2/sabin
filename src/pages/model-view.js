import { Typography, Row, Col, Tag, Input, Table, Avatar } from 'antd'
import Card from '../components/card'
import UserLayout from '../containers/UserLayout'
import SiderModel from '../containers/SiderModel'

const routeBuild = ['Training', 'Source', 'Stats', 'Report']
const routeRun = ['Demo', 'Batch']
const labelData = ['Positive', 'Negative']

const columns = ['Label', 'Persentage']

const dataSource = [
  {
    key: '1',
    name: 'Positive',
    result: '57%',
  },
  {
    key: '2',
    name: 'Negative',
    result: '53%',
  },
]

function ModelView(props) {
  const { name = 'Example Model Name', labels = labelData } = props
  return (
    <div className="myd-container">
      <div className="myd-container-item">
        <Row gutter={32}>
          <Col md={3}>
            <SiderModel />
          </Col>
          <Col md={21}>
            <Card style={{ marginBottom: 16 }}>
              <Row gutter={16}>
                <Col md={3}>
                  <Avatar icon="user" size={90} />
                </Col>
                <Col md={6}>
                  <Card.Title level={4}>{name}</Card.Title>
                </Col>
                <Col md={15}>
                  <Typography.Text>
                    Lorem Ipsum adalah contoh teks atau dummy dalam industri
                    percetakan dan penataan huruf atau typesetting. Lorem Ipsum
                    telah menjadi standar contoh teks sejak tahun 1500an.
                  </Typography.Text>
                  <div>
                    {labels.map(item => (
                      <Tag>{item}</Tag>
                    ))}
                  </div>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row gutter={16}>
                <Col md={15}>
                  <Typography.Title level={4}>Input text</Typography.Title>
                  <Input.TextArea rows={8} />
                </Col>
                <Col md={9}>
                  <Typography.Title level={4}>Result</Typography.Title>
                  <table>
                    <tr>
                      {columns.map(item => (
                        <th>{item}</th>
                      ))}
                    </tr>
                    {dataSource.map(item => (
                      <tr key={item.id}>
                        <td>{item.name}</td>
                        <td>{item.result}</td>
                      </tr>
                    ))}
                  </table>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </div>
    </div>
  )
}

ModelView.Layout = UserLayout

export default ModelView
