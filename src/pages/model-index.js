import { Typography, Row, Col, List, Avatar, Tag } from 'antd'
import Card from '../components/card'
import UserLayout from '../containers/UserLayout'
import SiderModel from '../containers/SiderModel'

const routeBuild = ['Training', 'Source', 'Stats', 'Report']
const routeRun = ['Demo', 'Batch', 'Stats']

function ModelIndex(props) {
  return (
    <div className="myd-container">
      <div className="myd-container-item">
        <Row gutter={32}>
          <Col md={3}>
            <SiderModel />
          </Col>
          <Col md={21}>
            <Card>
              <Row>
                <Col md={6}>
                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Avatar
                      size={112}
                      icon="user"
                      style={{ marginBottom: 16 }}
                    />
                    <Tag>Active</Tag>
                  </div>
                </Col>
                <Col md={18}>
                  <Typography.Title level={4}>Example Name</Typography.Title>
                  <div>
                    <h5>LABELS</h5>
                    {['Label1', 'Label1', 'Label1', 'Label1'].map(item => (
                      <Tag>{item}</Tag>
                    ))}
                  </div>
                  <div>
                    <h5>DESCRIPTION</h5>
                    <p>
                      Lorem Ipsum adalah contoh teks atau dummy dalam industri
                      percetakan dan penataan huruf atau typesetting. Lorem
                      Ipsum telah menjadi standar contoh teks sejak tahun
                      1500an, saat seorang tukang cetak yang tidak dikenal
                      mengambil sebuah kumpulan teks dan mengacaknya untuk
                      menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan
                      selama 5 abad, tapi juga telah beralih ke penataan huruf
                      elektronik, tanpa ada perubahan apapun. Ia mulai
                      dipopulerkan pada tahun 1960 dengan diluncurkannya
                      lembaran-lembaran Letraset yang menggunakan
                      kalimat-kalimat dari Lorem Ipsum, dan seiring munculnya
                      perangkat lunak Desktop Publishing seperti Aldus PageMaker
                      juga memiliki versi Lorem Ipsum.
                    </p>
                  </div>
                  <div>
                    <h5>RESULT</h5>
                    <p>
                      Lorem Ipsum adalah contoh teks atau dummy dalam industri
                      percetakan dan penataan huruf atau typesetting. Lorem
                      Ipsum telah menjadi standar contoh teks sejak tahun
                      1500an, saat seorang tukang cetak yang tidak dikenal
                      mengambil sebuah kumpulan teks dan mengacaknya untuk
                      menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan
                      selama 5 abad, tapi juga telah beralih ke penataan huruf
                      elektronik, tanpa ada perubahan apapun. Ia mulai
                      dipopulerkan pada tahun 1960 dengan diluncurkannya
                      lembaran-lembaran Letraset yang menggunakan
                      kalimat-kalimat dari Lorem Ipsum, dan seiring munculnya
                      perangkat lunak Desktop Publishing seperti Aldus PageMaker
                      juga memiliki versi Lorem Ipsum.
                    </p>
                  </div>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </div>
    </div>
  )
}

ModelIndex.Layout = UserLayout

export default ModelIndex
