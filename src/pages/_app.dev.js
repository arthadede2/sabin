import React from 'react'
import { Provider } from 'react-redux'
import App, { Container } from 'next/app'
import withRedux from 'next-redux-wrapper'
import initStore from '../store'
import DevTools from '../containers/DevTools'
import { hasToken } from '../utils/auth'

import '../assets/less/index.less'

class AnnoApp extends App {
  static async getInitialProps({ Component, ctx }) {
    const isAuth = hasToken(ctx)

    ctx.isAuth = isAuth

    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {}

    return { ...pageProps, isAuth }
  }

  render() {
    const { Component, isAuth, store, ...rest } = this.props
    return (
      <Container>
        <Provider store={store}>
          <div className="anno-content">
            <Component.Layout store={store} isAuth={isAuth} {...rest}>
              <Component isAuth={isAuth} {...rest} />
            </Component.Layout>
            <DevTools />
          </div>
        </Provider>
      </Container>
    )
  }
}

export default withRedux(initStore)(AnnoApp)
