import { Avatar } from 'antd'
import { connect } from 'react-redux'
import { ModelFilters, setModelFilter } from '../actions'
import Card from '../components/card'

const getVisibleModel = (models, filter) => {
  switch (filter) {
    case ModelFilters.SHOW_ALL:
      return models
    case ModelFilters.SHOW_CLASSIFIER:
      return models.filter(t => t.annotator === 'classifier')
    case ModelFilters.SHOW_EXTRACTOR:
      return models.filter(t => t.annotator === 'extractor')
    default:
      throw new Error('Unknown filter: ' + filter)
  }
}

function Classifier(props) {
  const { router, models } = props
  const currentRoute = router.asPath

  const handle = nextRoute => {
    router.push(`${currentRoute}/${nextRoute}`)
  }

  return (
    <div style={{ width: 900 }}>
      {models.map(m => (
        <Card key={m.id} row hoverable onClick={() => handle('intent')}>
          <Avatar
            size={60}
            src="/static/icon/intent.png"
            style={{ marginRight: 16 }}
          />
          <div style={{ marginLeft: 16 }}>
            <Card.Title>{m.name}</Card.Title>
            <Card.Description>{m.desc}</Card.Description>
          </div>
        </Card>
      ))}
    </div>
  )
}

Classifier.getInitialProps = async ({ store }) => {
  store.dispatch(setModelFilter(ModelFilters.SHOW_CLASSIFIER))

  return {}
}

const mapStateToProps = state => ({
  models: getVisibleModel(state.model.items, state.modelFilter),
})

export default connect(
  mapStateToProps,
  null
)(Classifier)
