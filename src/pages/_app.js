if (process.env.NODE_ENV === 'production') {
  module.exports = require('./_app.prod')
} else {
  module.exports = require('./_app.dev')
}
