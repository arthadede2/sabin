import { connect } from 'react-redux'
import { Row, Avatar, Col } from 'antd'
import Card from '../components/card'
import Button from '../components/button'
import Link from 'next/link'

function Annotator(props) {
  const { router } = props
  const currentRoute = router.asPath

  const handle = nextRoute => {
    return router.push({
      pathname: `/${currentRoute}/${nextRoute}`,
    })
  }

  return (
    <div
      style={{
        width: 900,
      }}
    >
      <Row type="flex" align="middle" justify="center" gutter={16}>
        <Col md={8}>
          <Card>
            <Avatar
              size={113}
              src="/static/icon/classifier.png"
              style={{ marginBottom: 16 }}
            />
            <Card.Title style={{ marginBottom: 8, marginTop: 16 }}>
              Classifier
            </Card.Title>
            <Card.Description
              align="center"
              style={{ marginBottom: 16, marginTop: 8 }}
            >
              Sudah merupakan fakta bahwa seorang pembaca akan terpengaruh oleh
              sesama aja.
            </Card.Description>
            <Link href="/annotation/classifier" as="/classifier">
              <Button type="outlined" style={{ marginTop: 16 }}>
                Mulai
              </Button>
            </Link>
          </Card>
        </Col>
        <Col md={8}>
          <Card>
            <Avatar
              size={113}
              src="/static/icon/extractor.png"
              style={{ marginBottom: 16 }}
            />
            <Card.Title style={{ marginBottom: 8, marginTop: 16 }}>
              Extractor
            </Card.Title>
            <Card.Description
              align="center"
              style={{ marginBottom: 16, marginTop: 8 }}
            >
              Sudah merupakan fakta bahwa seorang pembaca akan terpengaruh oleh
              sesama aja.
            </Card.Description>
            <Link href="/annotation/extractor" as="/extractor">
              <Button type="outlined" style={{ marginTop: 16 }}>
                Mulai
              </Button>
            </Link>
          </Card>
        </Col>
      </Row>
    </div>
  )
}
Annotator.getInitialProps = ({ store }) => {}

export default Annotator
