import React, { useState } from 'react'
import { Avatar, Radio } from 'antd'

import { Router } from '../routes'
import Card from '../components/card'
import Button from '../components/button'

const RadioGroup = Radio.Group

function DataSource(props) {
  const { query } = props
  const [selected, setSelected] = useState(null)

  const handleSelected = e => {
    setSelected(e.target.value)
  }

  const handleNext = () => {
    Router.pushRoute(selected, query)
  }

  return (
    <div style={{ width: 900, display: 'flex', flexDirection: 'column' }}>
      <RadioGroup onChange={handleSelected}>
        <Card
          type="radio"
          value="source-file"
          selected={selected === 'source-file'}
          row
          hoverable
          style={{ marginTop: 16, marginBottom: 16 }}
        >
          <Avatar
            size={60}
            src="/static/icon/xls.png"
            style={{ marginRight: 16 }}
          />
          <div style={{ marginLeft: 16 }}>
            <Card.Title>Import File</Card.Title>
            <Card.Description>
              Sudah merupakan fakta bahwa seorang pembaca akan terpengaruh oleh
              sesama aja.
            </Card.Description>
          </div>
        </Card>
        <Card
          type="radio"
          value="source-manual"
          selected={selected === 'source-manual'}
          row
          hoverable
          style={{ marginTop: 16, marginBottom: 16 }}
        >
          <Avatar
            size={60}
            src="/static/icon/add.png"
            style={{ marginRight: 16 }}
          />
          <div style={{ marginLeft: 16 }}>
            <Card.Title>Manual</Card.Title>
            <Card.Description>
              Sudah merupakan fakta bahwa seorang pembaca akan terpengaruh oleh
              sesama aja.
            </Card.Description>
          </div>
        </Card>
      </RadioGroup>
      <div>
        <Button onClick={handleNext}>Lanjutkan</Button>
      </div>
    </div>
  )
}

DataSource.getInitialProps = ({ query }) => {
  return { query }
}

export default DataSource
