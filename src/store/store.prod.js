import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from '../reducers'

function initializeStore(initializeState) {
  const store = createStore(
    rootReducer,
    initializeState,
    applyMiddleware(thunk),
  )

  return store
}

export default initializeStore
