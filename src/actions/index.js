import * as TYPES from '../constants'

export const setAuth = req => ({
  type: TYPES.SET_AUTH,
  req,
})

export const setModule = req => ({
  type: TYPES.SET_MODULE,
  req,
})

export const setModuleSelected = req => ({
  type: TYPES.SET_MODULE_SELECTED,
  req,
})

export const setModuleFilter = req => ({
  type: TYPES.SET_MODULE_FILTER,
  req,
})

export const setSource = req => ({
  type: TYPES.SET_SOURCE,
  req,
})

export const setSourceSelected = req => ({
  type: TYPES.SET_SOURCE_SELECTED,
  req,
})

export const setLabel = req => ({
  type: TYPES.SET_LABEL,
  req,
})

export const setLabelSelected = req => ({
  type: TYPES.SET_LABEL_SELECTED,
  req,
})

export const setModel = req => ({
  type: TYPES.SET_MODEL,
  req,
})

export const setModelSelected = req => ({
  type: TYPES.SET_MODEL_SELECTED,
  req,
})

export const setModelFilter = req => ({
  type: TYPES.SET_MODEL_FILTER,
  req,
})

export const setRole = req => ({
  type: TYPES.SET_ROLE,
  req,
})

export const setRoleSelected = req => ({
  type: TYPES.SET_ROLE_SELECTED,
  req,
})

export const setUser = req => ({
  type: TYPES.SET_USER,
  req,
})

export const setUserSelected = req => ({
  type: TYPES.SET_USER_SELECTED,
  req,
})

export const setUserFilter = req => ({
  type: TYPES.SET_USER_FILTER,
  req,
})
