import React, { useState, useRef } from 'react';
import { Classifier } from '../components/annotator';
import { Row, Col, Icon, Button, Typography, Checkbox, Input } from 'antd';
const CheckboxGroup = Checkbox.Group;

import EditIcon from '../static/svg/edit.svg';
import AddIcon from '../static/svg/add.svg';

const example =
    'Lorem Ipsum adalah contoh teks atau dummy dalam industri, Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat-kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.';

const labelExample = [
    {
        editable: false,
        text: 'Label',
    },
    {
        editable: false,
        text: 'Example1',
    },
    {
        editable: false,
        text: 'Example2',
    },
];

function AnnotationWithClassifier(props) {
    const [state, setState] = useState(example);
    const [label, setLabel] = useState(0);
    const [labelArr, setLabelArr] = useState(labelExample);
    const [labelEdit, setLabelEdit] = useState(false);
    const labelRef = useRef(null);

    const handleLabel = newVal => {
        setLabel(newVal);
    };

    const handleAddLabel = async () => {
        await setLabelArr(items => [
            ...items,
            {
                editable: true,
                text: `Label${labelArr.length + 1}`,
            },
        ]);
        await setLabelEdit(true);
        await labelRef.current.focus();
    };

    const handleEditLabel = status => {
        setLabelArr(items =>
            items.map(item => ({ ...item, editable: status })),
        );
        setLabelEdit(status);
    };

    const handleChangeLabel = async (newVal, i) => {
        setLabelArr(items => {
            const newState = items.map((item, j) => {
                if (j === i) {
                    item.text = newVal;
                    return item;
                }
                return item;
            });

            return newState;
        });
    };

    const handleRemoveLabel = async i => {
        setLabelArr(items => items.filter((item, j) => j !== i));
        handleEditLabel(false);
    };

    return (
        <div
            style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                minHeight: '100%',
                minWidth: '100%',
            }}
        >
            <div
                style={{
                    width: 900,
                    height: 460,
                    background: '#fafaff',
                    borderRadius: 5,
                    overflow: 'hidden',
                }}
            >
                <Row gutter={16} type="flex">
                    <Col lg={16}>
                        <div
                            style={{
                                height: 460,
                                background: '#fff',
                                boxShadow:
                                    '10px 0px 10px 1px hsla(0, 0%, 0%, 0.03)',
                                padding: 40,
                                paddingBottom: 80,
                            }}
                        >
                            <Classifier value={state} />
                        </div>
                        <div
                            style={{
                                position: 'absolute',
                                bottom: 0,
                                height: 80,
                                paddingLeft: 40,
                                paddingRight: 56,
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                width: '100%',
                            }}
                        >
                            <a href="#">SEBELUMNYA</a>
                            <Button
                                type="primary"
                                shape="circle"
                                icon="check"
                            />
                            <a href="#">LEWATKAN</a>
                        </div>
                    </Col>
                    <Col lg={8}>
                        <div
                            style={{
                                padding: '40px 30px',
                                paddingBottom: 80,
                                height: 460,
                                overflow: 'auto',
                            }}
                        >
                            <div
                                style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    marginBottom: 24,
                                }}
                            >
                                <Typography.Title
                                    level={4}
                                    style={{ marginBottom: 0 }}
                                >
                                    Label
                                </Typography.Title>
                                <div style={{ marginLeft: 'auto' }}>
                                    <Icon
                                        component={AddIcon}
                                        className="anno-card-header-action"
                                        style={{ fontSize: 11 }}
                                        onClick={handleAddLabel}
                                    />
                                    <Icon
                                        component={EditIcon}
                                        className="anno-card-header-action"
                                        onClick={() => handleEditLabel(true)}
                                    />
                                </div>
                            </div>
                            <CheckboxGroup onChange={handleLabel}>
                                {labelArr.map(({ text, editable }, index) => {
                                    if (editable === true) {
                                        return (
                                            <div
                                                className="anno-label-edit"
                                                key={index}
                                            >
                                                <Input
                                                    ref={labelRef}
                                                    className="anno-label-input"
                                                    type="text"
                                                    value={text}
                                                    onChange={e =>
                                                        handleChangeLabel(
                                                            e.target.value,
                                                            index,
                                                        )
                                                    }
                                                />
                                                <div className="anno-label-edit-action">
                                                    <Button
                                                        shape="circle"
                                                        icon="delete"
                                                        size="small"
                                                        type="danger"
                                                        onClick={() =>
                                                            handleRemoveLabel(
                                                                index,
                                                            )
                                                        }
                                                    />
                                                </div>
                                            </div>
                                        );
                                    }
                                    return (
                                        <Checkbox
                                            key={index}
                                            style={{
                                                display: 'block',
                                                height: 35,
                                                marginLeft: 0,
                                            }}
                                            value={text}
                                        >
                                            {text}
                                        </Checkbox>
                                    );
                                })}
                            </CheckboxGroup>
                        </div>
                        <div className="anno-label-action">
                            {labelEdit === true && (
                                <div className="anno-label-action-edit">
                                    <Button
                                        type="primary"
                                        shape="circle"
                                        icon="check"
                                        onClick={() => handleEditLabel(false)}
                                    />
                                </div>
                            )}
                        </div>
                    </Col>
                </Row>
            </div>
            <style global jsx>{`
                .anno-card-header-action {
                    color: #b2bec3;
                    margin-left: 24px;
                    cursor: pointer;
                    transition: color 0.3s ease;
                }
                .anno-card-header-action:hover {
                    color: #333;
                }

                .anno-label-action {
                    position: absolute;
                    bottom: 0;
                    left: -8px;
                    height: 80px;
                    width: 100%;
                    padding: 0px 30px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                }
                .anno-label-edit {
                    position: relative;
                    padding-right: 40px;
                    margin-bottom: 8px;
                }
                .anno-label-edit-action {
                    position: absolute;
                    top: 0;
                    right: 0;
                    width: 30px;
                    height: 32px;
                    display: flex;
                    align-items: center;
                }
            `}</style>
        </div>
    );
}

export default AnnotationWithClassifier;
