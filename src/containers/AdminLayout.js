import React, { useEffect } from 'react'
import { PageHeader, Layout, Button } from 'antd'
import Header from '../components/header'
import Content from '../components/content'
import Sider from '../components/sider'
import { Router } from '../routes'
import { logout } from '../utils/auth'
import { Auth } from '../services'
import { setAuth } from '../actions'

export default ({ isAuth, isServer, store, children, ...rest }) => {
  const { auth } = store.getState()

  const handleLogin = () => Router.pushRoute('/login')

  const handleLogout = () => {
    logout()
    store.dispatch(setAuth({}))
    Router.pushRoute('/')
  }

  const MenuBasic = (
    <>
      <Button type="primary" onClick={handleLogin}>
        Login
      </Button>
    </>
  )
  const MenuAuth = (
    <>
      <Button type="primary" onClick={handleLogout}>
        Logout
      </Button>
    </>
  )

  return (
    <Layout className="myd-layout">
      <Header items={!isAuth ? MenuBasic : MenuAuth} />
      <Layout className="myd-layout-wrapper">
        <Sider />
        <Content>{children}</Content>
      </Layout>
    </Layout>
  )
}
