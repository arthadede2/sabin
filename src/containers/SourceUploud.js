import React, { useContext } from 'react'
import {
  Typography,
  Icon,
  Upload,
  Row,
  Col,
  Progress,
  Divider,
  Checkbox,
  Empty,
  Button,
} from 'antd'

function FileListEmpty() {
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: '100%',
      }}
    >
      <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
      <style global jsx>{`
        .ant-empty-image {
          height: 50px;
        }
      `}</style>
    </div>
  )
}

function FileList(props) {
  const { value: state, onChange: setState } = props

  function formatBytes(bytes, decimals = 2) {
    if (bytes == 0) return '0 Bytes'
    var k = 1024,
      sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
      i = Math.floor(Math.log(bytes) / Math.log(k))
    return (
      parseFloat((bytes / Math.pow(k, i)).toFixed(decimals)) + ' ' + sizes[i]
    )
  }

  const progress = a => Number(a.toFixed(0))
  const progressSize = (progress, size) => {
    const current = formatBytes((progress / 100) * size)
    const total = formatBytes(size)

    return `${current} / ${total}`
  }

  const handleRemove = i => {
    setState(state => ({
      ...state,
      fileList: state.fileList.filter((item, j) => j !== i),
    }))
  }

  return (
    <div
      style={{
        width: '100%',
        height: 400,
        display: 'flex',
        flexDirection: 'column',
        padding: '40px 40px',
      }}
    >
      <div style={{ overflow: 'auto' }}>
        {state.fileList.map((item, index) => {
          const { name, percent, size } = item

          return (
            <div key={index}>
              <div
                style={{
                  position: 'relative',
                  display: 'flex',
                  alignItems: 'center',
                  margin: '24px 16px',
                  overflow: 'hidden',
                  paddingRight: 30,
                }}
              >
                <Progress
                  type="circle"
                  percent={progress(percent)}
                  width={42}
                  strokeWidth={8}
                />
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    marginLeft: 30,
                    marginRight: 30,
                    overflow: 'hidden',
                  }}
                >
                  <Typography.Text
                    strong
                    style={{
                      textOverflow: 'ellipsis',
                      whiteSpace: 'nowrap',
                      overflow: 'hidden',
                      width: '100%',
                      color: '#2d3436',
                      letterSpacing: 0.2,
                    }}
                  >
                    {name}
                  </Typography.Text>
                  <Typography.Text
                    type="secondary"
                    style={{
                      textOverflow: 'ellipsis',
                      whiteSpace: 'nowrap',
                      overflow: 'hidden',
                      width: '100%',
                    }}
                  >
                    {progressSize(percent, size)}
                  </Typography.Text>
                </div>
                <div
                  style={{
                    position: 'absolute',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    right: 0,
                    top: 0,
                    bottom: 0,
                    width: 30,
                  }}
                >
                  <Button
                    type="danger"
                    icon="close"
                    shape="circle"
                    size="small"
                    onClick={() => handleRemove(index)}
                  />
                </div>
              </div>
              <Divider />
            </div>
          )
        })}
      </div>
    </div>
  )
}

function SourceUploud(props) {
  const { value: state, onChange: setState } = props

  const handleUploud = info => {
    setState(state => ({
      ...state,
      fileList: [...info.fileList],
    }))
  }

  const optionsDragger = {
    name: 'file',
    multiple: true,
    showUploadList: false,
    fileList: state.fileList,
    onChange: handleUploud,
    style: {
      background: '#fff',
      border: 'none',
      outline: 'none',
      minHeight: 400,
      boxShadow: '10px 0px 10px 1px hsla(0, 0%, 0%, 0.03)',
      zIndex: 1,
    },
  }

  return (
    <>
      <Row style={{ background: '#fafaff' }} type="flex">
        <Col lg={12} xs={24}>
          <Upload.Dragger {...optionsDragger}>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <img
                src="/static/icon/uploud.png"
                alt=""
                style={{ width: 60, marginBottom: 16 }}
              />
              <Typography.Text>Drag and drop files here</Typography.Text>
            </div>
          </Upload.Dragger>
        </Col>
        <Col lg={12} xs={24}>
          {state.fileList.length === 0 && <FileListEmpty />}
          {state.fileList.length !== 0 && (
            <FileList value={state} onChange={setState} />
          )}
        </Col>
      </Row>
      <style global jsx>{`
        .ant-upload {
          border-radius: 5px 0px 0px 5px !important;
        }
        .ant-upload-drag {
          border: none !important;
          outline: none !important;
          border-radius: 5px 0px 0px 5px !important;
        }
      `}</style>
    </>
  )
}

SourceUploud.getInitialProps = props => {
  return {}
}

export default SourceUploud
