import { Layout } from 'antd'

export default ({ children }) => {
  return (
    <Layout className="myd-layout">
      <div className="myd-layout-wrapper myd-layout-wrapper-center">{children}</div>
    </Layout>
  )
}
