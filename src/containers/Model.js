// import { kebabCase } from 'lodash'
// import React, { useState } from 'react'
// import { connect } from 'react-redux'
// import { Input, Row, Col, Avatar, Radio } from 'antd'
// import Card from '../components/card'
// import Button from '../components/button'
// import { Router } from '../routes'

// const RadioGroup = Radio.Group
// const RadioButton = Radio.Button

// function Extractor(props) {
//   const { models, setFilter, setModel } = props
//   const [selected, setSelected] = useState(0)

//   const handleFilter = e => {
//     setFilter(e.target.value)
//   }

//   const handleSelected = e => {
//     setSelected(e.target.value)
//   }

//   const handleNext = () => {
//     Router.pushRoute('source', {
//       model: kebabCase(models[selected].name),
//     })
//   }

//   return (
//     <div style={{ width: 900, display: 'flex', flexDirection: 'column' }}>
//       <div>
//         <Input.Search
//           placeholder="Pencarian"
//           onSearch={value => console.log(value)}
//           style={{
//             marginBottom: 60,
//           }}
//         />
//       </div>
//       <div>
//         <RadioGroup
//           onChange={handleFilter}
//           defaultValue={ModelFilters.SHOW_ALL}
//         >
//           <RadioButton value={ModelFilters.SHOW_ALL}>All</RadioButton>
//           <RadioButton value={ModelFilters.SHOW_CLASSIFIER}>
//             Classifier
//           </RadioButton>
//           <RadioButton value={ModelFilters.SHOW_EXTRACTOR}>
//             Extractor
//           </RadioButton>
//         </RadioGroup>
//       </div>
//       <div>
//         <RadioGroup onChange={handleSelected} style={{ width: '100%' }}>
//           <Row gutter={16}>
//             {models.map((model, index) => (
//               <Col md={8} xs={12} key={model.id}>
//                 <Card
//                   type="radio"
//                   value={index}
//                   hoverable
//                   selected={index === selected}
//                   style={{ maxWidth: 217.406 }}
//                 >
//                   <Avatar
//                     size={113}
//                     src="/static/icon/classifier.png"
//                     style={{ marginBottom: 16 }}
//                   />
//                   <Card.Title
//                     style={{
//                       marginBottom: 8,
//                       marginTop: 16,
//                     }}
//                   >
//                     {model.name}
//                   </Card.Title>
//                   <Card.Description
//                     align="center"
//                     style={{
//                       marginBottom: 16,
//                       marginTop: 8,
//                       maxHeight: 63,
//                     }}
//                   >
//                     {model.desc}
//                   </Card.Description>
//                 </Card>
//               </Col>
//             ))}
//           </Row>
//         </RadioGroup>
//       </div>
//       <div>
//         <Button onClick={handleNext}>Lanjutkan</Button>
//       </div>
//       <style jsx>{``}</style>
//     </div>
//   )
// }

// const mapStateToProps = state => ({
//   models: getModelVisibility(state.model.items, state.modelFilter),
// })

// const mapDispatchToProps = dispatch => ({
//   setFilter: filter => dispatch(setModelFilter(filter)),
//   setModel: model => dispatch(setAnnotationModel(model)),
// })

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps,
// )(Extractor)

import React from 'react'

function Model() {
  return (
    <div>
      
    </div>
  )
}

export default Model
