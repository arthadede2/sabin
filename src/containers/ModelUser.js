import React, { useEffect } from 'react'
import { Router } from '../routes'
import {
  Typography,
  Row,
  Col,
  Avatar,
  List,
  Tag,
  Button,
  Icon,
  Dropdown,
  Menu,
} from 'antd'
import Card from '../components/card'
import UserLayout from '../containers/UserLayout'
import MoreIcon from '../static/svg/more.svg'

const labelExample = [
  'Label',
  'Label1',
  'Label2',
  'Label1',
  'Label2',
  'Label1',
  'Label2',
  'Label1',
  'Label2',
]

const modelDescription =
  'Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf.'

function ModelUser(props) {
  const modelArr = props.data
  const handleShow = () => {
    Router.pushRoute('/model-view')
  }
  const menu = (
    <Menu>
      <Menu.Item key="0">
        <a onClick={handleShow}>Show</a>
      </Menu.Item>
      <Menu.Item key="1">
        <a href="http://www.taobao.com/">Edit</a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="3">Remove</Menu.Item>
    </Menu>
  )

  return (
    <List
      itemLayout="vertical"
      dataSource={modelArr}
      renderItem={item => (
        <List.Item>
          <Card style={{ width: '100%' }}>
            <Row gutter={16} type="flex">
              <Col md={4}>
                <div
                  style={{
                    height: '100%',
                    width: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <Avatar size={93} icon="user" />
                </div>
              </Col>
              <Col md={18}>
                <Typography.Title level={4}>{item.name}</Typography.Title>
                <Typography.Text>{modelDescription}</Typography.Text>
                <div>
                  {labelExample.map(item => (
                    <Tag>{item}</Tag>
                  ))}
                </div>
              </Col>
              <Col md={2}>
                <div
                  style={{
                    height: '100%',
                    width: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <Dropdown overlay={menu} trigger={['click']}>
                    <a href="#">
                      <Icon component={MoreIcon} style={{ fontSize: 24 }} />
                    </a>
                  </Dropdown>
                </div>
              </Col>
            </Row>
          </Card>
        </List.Item>
      )}
    />
  )
}

ModelUser.Layout = UserLayout

export default ModelUser
