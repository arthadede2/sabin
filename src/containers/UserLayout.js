import React, { useEffect } from 'react'
import { PageHeader, Layout, Button } from 'antd'
import Header from '../components/header'
import Content from '../components/content'
import Sider from '../components/sider'
import { Router } from '../routes'
import { removeToken } from '../utils/auth'
import { setAuth } from '../actions'

export default ({ isAuth, isServer, store, children, ...rest }) => {
  const handleLogin = () => Router.pushRoute('/login')

  const handleLogout = () => {
    removeToken()
    store.dispatch(setAuth({}))
    Router.pushRoute('/')
  }

  const MenuBasic = (
    <>
      <Button type="primary" onClick={handleLogin}>
        Login
      </Button>
    </>
  )
  const MenuAuth = (
    <>
      <Button type="primary" onClick={handleLogout}>
        Logout
      </Button>
    </>
  )

  return (
    <Layout className="myd-layout">
      <Layout className="myd-layout-wrapper">
        <Header items={!isAuth ? MenuBasic : MenuAuth} />
        <Content>{children}</Content>
      </Layout>
    </Layout>
  )
}
