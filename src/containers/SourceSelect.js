import React, { useEffect } from 'react'
import Router from 'next/router'
import { Typography, Checkbox } from 'antd'
import Button from '../components/button'
import XLSX from 'xlsx'
import GenerateKeys from '../utils/generate-column'

const CheckboxGroup = Checkbox.Group

const splitByRow = w => {
  const { Sheets } = w
  let result = {}

  for (const key in Sheets) {
    const current = Sheets[key]

    if (current['!ref']) {
      let data = []
      const sheetOfKeys = GenerateKeys(current['!ref'])
      const [start, end] = current['!ref'].split(':').map(val => {
        return parseInt(val.match(/\d/g).join(''), 10)
      })

      for (let i = start; i <= end; i++) {
        let dataOfRow = []
        sheetOfKeys.map(ref => {
          const refKey = ref + i
          if (current.hasOwnProperty(refKey)) {
            return dataOfRow.push(current[refKey].w)
          }
          return dataOfRow.push(null)
        })
        data.push(dataOfRow)
      }

      result[key] = data
    }
  }
  return result
}

const fileReader = f => {
  return new Promise((resolve, reject) => {
    let reader = new FileReader()

    reader.onload = function(e) {
      let data = new Uint8Array(e.target.result)
      let workbook = XLSX.read(data, { type: 'array' })
      const result = splitByRow(workbook)
      resolve(result)
    }
    reader.onerror = reject
    reader.readAsArrayBuffer(f)
  })
}

const getSheet = f => f.sheets
const getColumn = s => s.length
const getTotalColumn = files => {
  let total = 0

  files.map(file => {
    const sheets = getSheet(file)

    for (const sheet in sheets) {
      const currentSheet = sheets[sheet]
      currentSheet.forEach(sheet => {
        const totalColumn = getColumn(sheet)

        if (total < totalColumn) total = totalColumn
      })
    }
  })

  return Array.from({ length: total }, (v, k) => k)
}

const getSheetByColumn = (files, totalCol) => {
  let result = []
  for (let i = 0; i < totalCol; i++) {
    let resultCol = []

    for (const file of files) {
      const sheet = getSheet(file)

      for (const key in sheet) {
        const currentSheet = sheet[key]

        if (currentSheet[i]) resultCol.push(currentSheet[i])
      }
    }
    result.push(resultCol)
  }
  return result
}

const getSheetByRow = (files, totalCol) => {
  let result = []

  for (const file of files) {
    const sheet = getSheet(file)

    for (const key in sheet) {
      const currentSheet = sheet[key]
      for (const s of currentSheet) {
        result.push(s)
      }
    }
  }

  return result
}

function SourceSelect(props) {
  const { value: state, onChange: setState } = props
  const totalColumn = getTotalColumn(state.fileList)
  const tempSheet = getSheetByRow(state.fileList, totalColumn)

  const readFile = async () => {
    let files = state.fileList
    let newState = []

    try {
      for (let i = 0; i < files.length; i++) {
        const currentFile = files[i]

        if (!currentFile.hasOwnProperty('sheets')) {
          let resultReader = await fileReader(currentFile.originFileObj)
          currentFile.sheets = resultReader
        }
        newState.push(currentFile)
      }
      setState(state => ({
        ...state,
        fileList: newState,
      }))
    } catch (err) {
      throw err
    }
  }

  useEffect(() => {
    readFile()
  }, [])

  useEffect(() => {
    console.log(state)
  }, [state])

  useEffect(() => {
    console.log(tempSheet)
  }, [tempSheet])

  const handleSelect = val => {
    setState(state => ({
      ...state,
      selected: val,
    }))
  }

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: '100%',
        minWidth: '100%',
      }}
    >
      <div
        style={{
          width: 900,
        }}
      >
        <Typography.Title level={4}>Select Columns</Typography.Title>
        <div
          style={{
            background: '#fff',
            borderRadius: 5,
            height: 400,
            boxShadow: '0px 0px 15px 1px hsla(0, 0%, 0%, 0.09)',
            overflow: 'auto',
          }}
        >
          <section className="anno-table-select">
            <header className="anno-table-select-row">
              <CheckboxGroup
                onChange={val => handleSelect(val)}
                style={{ width: '100%', display: 'contents' }}
              >
                <div
                  className="anno-table-select-header"
                  style={{ minWidth: 30, width: 30, borderBottom: 'none' }}
                />
                {totalColumn.map((item, index) => (
                  <div key={index} className="anno-table-select-header">
                    <Checkbox
                      value={index}
                      style={{
                        fontSize: 12,
                        fontWeight: 600,
                        color: '#333',
                      }}
                    >
                      Use this column
                    </Checkbox>
                  </div>
                ))}
              </CheckboxGroup>
            </header>
            {tempSheet.map((item, index) => (
              <div key={index} className="anno-table-select-row">
                <div
                  className="anno-table-select-col"
                  style={{ textAlign: 'center' }}
                >
                  {index + 1}
                </div>
                {totalColumn.map(col => {
                  if (item[col] !== null && item[col] !== undefined) {
                    return (
                      <div className="anno-table-select-col">
                        <span className="anno-table-select-body-text">
                          {item[col]}
                        </span>
                      </div>
                    )
                  }
                  return (
                    <div className="anno-table-select-col">
                      <span className="anno-table-select-body-text-empty">
                        -
                      </span>
                    </div>
                  )
                })}
              </div>
            ))}
          </section>
        </div>
      </div>
      <style jsx>{`
        .anno-table-select-checkbox {
        }
        .anno-table-select {
          display: table;
          table-layout: fixed;
          min-width: 100%;
          position: relative;
        }
        .anno-table-select-row {
          display: table-row;
          overflow: auto;
        }
        .anno-table-select-header {
          text-align: center;
          height: 44px;
          min-width: 180px;
          vertical-align: middle;
          display: table-cell;
          padding: 8px;
          border-bottom: solid 0.3px #e6e6e6;
          border-left: solid 0.3px #e6e6e6;
        }
        .anno-table-select-col {
          display: table-cell;
          overflow: hidden;
          border-left: solid 0.3px #e6e6e6;
          padding: 6px 16px;
        }
        .anno-table-select-body-text {
          height: auto;
          font-size: 12px;
          display: -webkit-box;
          text-overflow: ellipsis;
          -webkit-line-clamp: 3;
          -webkit-box-orient: vertical;
          overflow: hidden;
        }
        .anno-table-select-body-text-empty {
          height: auto;
          font-size: 12px;
          display: -webkit-box;
          text-overflow: ellipsis;
          -webkit-line-clamp: 3;
          -webkit-box-orient: vertical;
          overflow: hidden;
          text-align: center;
        }
        .anno-table-select-row div:nth-child(1) {
          background: #f5fbfc;
          padding: 0px;
          font-size: 12px;
        }
      `}</style>
    </div>
  )
}

export default SourceSelect
