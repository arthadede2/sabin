import { List, Button } from 'antd'
import { Router } from '../routes'

const routes = {
  routeIndex: [
    {
      name: 'model-index',
      route: '/model-index',
      text: 'Info Model',
    },
  ],
  routeRun: [
    {
      name: 'model-view',
      route: '/model-view',
      text: 'Demo',
    },
    {
      name: 'model-batch',
      route: '/model-batch',
      text: 'Batch',
    },
    {
      name: 'model-stats',
      route: '/model-stats',
      text: 'Stats',
    },
  ],
  routeBuild: [
    {
      name: 'model-training',
      route: '/model-training',
      text: 'Training',
    },
    {
      name: 'model-data',
      route: '/model-data',
      text: 'Data',
    },
    {
      name: 'model-stats',
      route: '/model-stats',
      text: 'Stats',
    },
  ],
}

function SiderModel(props) {
  return (
    <div>
      <List
        dataSource={routes.routeIndex}
        renderItem={item => (
          <List.Item>
            <a onClick={() => Router.pushRoute(item.route)}>{item.text}</a>
          </List.Item>
        )}
      />
      <List
        header={<div>Run</div>}
        dataSource={routes.routeRun}
        renderItem={item => (
          <List.Item>
            <a onClick={() => Router.pushRoute(item.route)}>{item.text}</a>
          </List.Item>
        )}
      />
      <List
        header={<div>Build</div>}
        dataSource={routes.routeBuild}
        renderItem={item => (
          <List.Item>
            <a onClick={() => Router.pushRoute(item.route)}>{item.text}</a>
          </List.Item>
        )}
      />
    </div>
  )
}

export default SiderModel
