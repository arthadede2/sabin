import { Component } from 'react'
import Cookies from 'js-cookie'
import nextCookie from 'next-cookies'
import jwt from 'jsonwebtoken'
import { setAuth } from '../actions'
import { Router } from '../routes'

export const setToken = token => {
  Cookies.set('token', token, { expires: 1 })
}

export const removeToken = () => {
  Cookies.remove('token')
}

const getDisplayName = Component => Component.displayName || Component.name || 'Component'

export const hasToken = ctx => {
  const { token } = nextCookie(ctx)
  if (token) {
    try {
      const jwtPayload = jwt.verify(token, 'SECRET')
      ctx.store.dispatch(setAuth(jwtPayload))
      return true
    } catch (err) {
      removeToken()
    }
  }

  return false
}

export const withAuth = WrappedComponent =>
  class extends Component {
    static displayName = `withAuthSync(${getDisplayName(WrappedComponent)})`

    static async getInitialProps(ctx) {
      const token = await auth(ctx)

      const componentProps = WrappedComponent.getInitialProps && (await WrappedComponent.getInitialProps(ctx))

      return { ...componentProps, token }
    }

    constructor(props) {
      super(props)
    }

    render() {
      return <WrappedComponent {...this.props} />
    }
  }

export const auth = ctx => {
  const token = getToken()

  if (ctx.req && !token) {
    ctx.res.writeHead(302, { Location: '/login' })
    ctx.res.end()
    return
  }

  if (!token) {
    Router.pushRoute('/login')
  }

  try {
    const jwtPayload = jwt.verify(token, 'SECRET')
    ctx.dispatch(setAuth(jwtPayload))
  } catch (err) {
    removeToken()
    Router.pushRoute('/login')
  }

  return token
}
