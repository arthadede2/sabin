'use strict'

const arrayOfSentence = val => val.split(' ')

const hasSpecialCharacter = val => val.match(/[$-/:-?{-~!"^_`\[\]]/g)

const parsingSymbol = val => {
  let result = []
  let valArr = val.split(' ')
  result = valArr.map(item => {
    let hasSP = hasSpecialCharacter(item)
    if (hasSP !== null) {
      hasSP.forEach(sp => {
        item = item.split(sp).join(` ${sp} `)
      })
    }
    return item
  })

  return result.join(' ')
}

function Parser(source) {
  this.source = source

  const result = parsingSymbol(source)
  let arrayString = arrayOfSentence(result)
  arrayString = arrayString.filter(item => item !== null && item !== '')

  return arrayString.map(item => ({ text: [item] }))
}

export default Parser
