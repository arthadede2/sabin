import * as TYPES from '../constants'

const initialState = {
  selected: null,
  items: [],
  filter: null,
}

function userApp(state = initialState, action) {
  switch (action.type) {
    case TYPES.SET_USER:
      return {
        ...state,
        items: action.body,
      }
    case TYPES.SET_USER_SELECTED:
      return {
        ...state,
        selected: action.body,
      }
    case TYPES.SET_USER_FILTER:
      return {
        ...state,
        filter: action.body,
      }
    default:
      return state
  }
}

export default userApp
