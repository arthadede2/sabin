import * as TYPES from '../constants'

const initialState = {
  selected: null,
  items: [],
  filter: null,
}

function module(state = initialState, action) {
  switch (action.type) {
    case TYPES.SET_MODULE:
      return {
        ...state,
        items: action.body,
      }
    case TYPES.SET_MODULE_SELECTED:
      return {
        ...state,
        selected: action.body,
      }
    case TYPES.SET_MODULE_FILTER:
      return {
        ...state,
        filter: action.body,
      }
    default:
      return state
  }
}

export default module
