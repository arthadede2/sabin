import * as TYPES from '../constants'

const initialState = {
  selected: null,
  items: [],
}

function sourceApp(state = initialState, action) {
  switch (action.type) {
    case TYPES.SET_SOURCE:
      return {
        ...state,
        items: action.body,
      }
    case TYPES.SET_SOURCE_SELECTED:
      return {
        ...state,
        selected: action.body,
      }
    default:
      return state
  }
}

export default sourceApp
