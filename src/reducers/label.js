import * as TYPES from '../constants'

const initialState = {
  selected: null,
  items: [],
}

function labelApp(state = initialState, action) {
  switch (action.type) {
    case TYPES.SET_LABEL:
      return {
        ...state,
        items: action.body,
      }
    case TYPES.SET_LABEL_SELECTED:
      return {
        ...state,
        selected: action.body,
      }
    default:
      return state
  }
}

export default labelApp
