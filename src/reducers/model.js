import * as TYPES from '../constants'

const initialState = {
  selected: null,
  items: [],
  filter: null,
}

function modelApp(state = initialState, action) {
  switch (action.type) {
    case TYPES.SET_MODEL:
      return {
        ...state,
        items: action.req,
      }
    case TYPES.SET_MODEL_SELECTED:
      return {
        ...state,
        selected: action.req,
      }
    case TYPES.SET_MODEL_FILTER:
      return {
        ...state,
        filter: action.req,
      }
    default:
      return state
  }
}

export default modelApp
