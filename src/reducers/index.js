import { combineReducers } from 'redux'
import model from './model'
import module from './module'
import label from './label'
import role from './role'
import source from './source'
import user from './user'
import auth from './auth'

const rootReducer = combineReducers({
  model,
  module,
  label,
  role,
  source,
  user,
  auth,
})

export default rootReducer
