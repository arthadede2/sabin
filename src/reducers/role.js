import * as TYPES from '../constants'

const initialState = {
  selected: null,
  items: [],
}

function roleApp(state = initialState, action) {
  switch (action.type) {
    case TYPES.SET_ROLE:
      return {
        ...state,
        items: action.body,
      }
    case TYPES.SET_ROLE_SELECTED:
      return {
        ...state,
        selected: action.body,
      }
    default:
      return state
  }
}

export default roleApp
