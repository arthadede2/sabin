/* eslint-disable */
const withLess = require('@zeit/next-less')
const withOffline = require('next-offline')

module.exports = withOffline(
  withLess({
    target: 'serverless',
    lessLoaderOptions: {
      cssModules: true,
      javascriptEnabled: true,
    },
    webpack: (config, options) => {
      config.module.rules.push({
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          options.defaultLoaders.babel,
          {
            loader: '@svgr/webpack',
            options: {
              babel: false,
              icon: true,
            },
          },
        ],
      })

      return config
    },
  }),
)
